"abilities"
{
"1"	"abaddon_soul_rip"
"2"	"ice_path"
"3"	"earthshaker_enchant_totem_lua"
"4"	"zuus_arc_lightning_lua"
"5"	"blink_support"
"6" "hex_6_gon"
"7 " "fire_wall"
"8 " "winds"
"9 " "teleport_basic"
"10" "shadow_walk" //invisibility with greater speed at night time
"11" "holy_word" //Aoe heal, more heal at day time
"12" "summon_basic"
"13" "double_step"
"14" "eldri_anti_magic"
"15" "zombie_ambush"
"16" "magic_missile"
"17" "purge_burn" //Random effect more likely to be positive if at low health. Hidden extra if lot of players pray at same time.
"18" "vortex_blink"
"19" "sun_ray"
"20" "cursed_power"
"21" "forced_overload"
"22" "spike_hide"
"23" "eat_tree_eldri"
"24" "lightning_rod"
"25" "grow_beard"
"26" "poison_cloud"
"27" "earthshaker_fissure_lua"
"28" "black_dust"
"29"	"omni_immunity"
"30"	"missile_fire"
"31"	"missile_shock"
"32"	"channel_earthquake"
"33"	"missile_eldri"
"34" "swift" //half done (effect needed)
"35"	"nth_attack" //done (needs name)
"36"	"earthshaker_aftershock_lua" //done
"37"	"berserker" //
"38"	"zuus_static_field_lua" //done
"39" "collapsing_armor" // done
"40" "reactive_armor" // done
"41" "burner" // effects needed
"42" "healer" // effects needed
"43" "mana_well" // effects needed
"44" "evasion" // effects needed
"45" "critical" // effects needed
"46" "cleave"
"47" "poison_weapon"
"48" "vampiric"
"49" "chi_strike"
"50" "day_mana"
"51" "night_mana"
"52" "spell_resistance"
"53" "armored"
"54" "shock_weapon"
"55" "fire_weapon"
"56" "attack_stun"
"57" "defence_stun"
"58" "aura_vampire"
"59" "aura_speed"
"60"  "spell_reflect"
"61"  "eldri_step"

//"" "contract" //fire orb
//"" "runner"
//"" "reflexes"
//"" "eldri_focus"
//"" "greed_m"
//"" "greed_s"
//"" "war_drum"
//"" "alertillery" //target half way point. friendly fire possible.

//"665" "dev_target"
//"666" "dev_test"
}
