if element_poison == nil then
	element_poison = class({})
end

function element_poison:OnCreated( kv )	
	if IsServer() then
		self:SetStackCount(kv.stacks)
		self.nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_broodmother/broodmother_poison_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( self.nFXIndex, false, false, -1, false, false )
		self:CalculateDuration()
		self:StartIntervalThink(1)
	end
end

function element_poison:OnRefresh( kv )	
	if IsServer() then
		local stacks = self:GetStackCount() + kv.stacks
		self:SetStackCount(stacks)
		self:CalculateDuration()
	end
end

function element_poison:CalculateDuration()
	self:SetDuration( self:GetStackCount(), true )
end

function element_poison:GetTexture()
	return "element_poison"
end

function element_poison:OnIntervalThink()
	if IsServer() then
		local tKeys = self:CheckPoisons()
		local damageTable = {
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = tKeys.damage,
			damage_type = tKeys.damage_type
		}
		local dmg = ApplyDamage( damageTable )
		if dmg > 0 then
			local life_time = 2.0
			local digits = string.len( math.floor( dmg ) ) + 1
			local numParticle = ParticleManager:CreateParticle( "particles/msg_fx/msg_crit.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
			ParticleManager:SetParticleControl( numParticle, 1, Vector( 0, dmg, 5 ) )
			ParticleManager:SetParticleControl( numParticle, 2, Vector( life_time, digits, 0 ) )
			ParticleManager:SetParticleControl( numParticle, 3, Vector( tKeys.color_r, tKeys.color_g, tKeys.color_b ) )
		end
		if self:GetStackCount() < 1 then
			self:Destroy()
		end
		self:DecrementStackCount()
	end
end

function element_poison:CheckPoisons()
	local hTarget = self:GetParent()
	local tModifiers = hTarget:FindAllModifiers()
	local tKeys = {
	damage = 20,
	damage_type = DAMAGE_TYPE_PHYSICAL,
	stack = self:GetStackCount(),
	color_r = 0,
	color_g = 200,
	color_b = 0
	}
	for __,hModifier in pairs(tModifiers) do
		if hModifier.OnPoisonThink then
			tKeys = hModifier:OnPoisonThink(tKeys)
		end
	end
	return tKeys
end

function element_poison:OnDestroy()
	if IsServer() then
		local hTarget = self:GetParent()
		local tModifiers = hTarget:FindAllModifiers()
		for __,hModifier in pairs(tModifiers) do
			if hModifier.OnPoisonThink then
				hModifier:Destroy()
			end
		end
	end
end

function element_poison:IsHidden()
	return false
end

function element_poison:IsPurgable() 
	return true
end

function element_poison:DestroyOnExpire()
	return true
end