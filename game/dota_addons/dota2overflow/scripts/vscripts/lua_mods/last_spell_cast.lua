if my_passive_lua_modifier == nil then
	my_passive_lua_modifier = class({})
end

function my_passive_lua_modifier:OnCreated()
	if IsServer() then
		self.units = {}
	end
end

function my_passive_lua_modifier:DeclareFunctions()
	local funcs = {
	MODIFIER_EVENT_ON_ABILITY_EXECUTED
	}
	return funcs
end

function my_passive_lua_modifier:OnAbilityExecuted(kv)
	if IsServer() then
		if kv.ability:IsItem() then return end
		if kv.unit:GetTeam() ~= self:GetParent():GetTeam() then
			self.units[kv.unit:entindex()] = GetAbilityIndex()
		end
	end
end

function my_passive_lua_modifier:RequestLastSpell(iIndex)
	if IsServer() then
		if self.units[iIndex] ~= nil then
			return self.units[iIndex]
		else
			return -1
		end
	end
end

--assuming lua ability so self refers to handle of the ability
--local hTarget = self:GetCursorTarget()
--local iAbility = self:GetCaster():FindModifierByName("my_passive_lua_modifier"):RequestLastSpell(hTarget:entindex())
--local hAbility = hTarget:GetAbilityByIndex(iAbility)
--local iManaCost = hAbility:GetManaCost(hAbility:GetLevel())
