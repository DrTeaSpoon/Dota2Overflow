if element_elec == nil then
	element_elec = class({})
end

function element_elec:OnCreated( kv )
	if IsServer() then
		self:SetStackCount(kv.stacks)
		self.nFXIndex = ParticleManager:CreateParticle( "particles/elec_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( self.nFXIndex, false, false, -1, false, false )
		self:SetDuration(self:CalcDuration(),true)
		self:CheckConversions()
	end
end

function element_elec:OnRefresh( kv )
	if IsServer() then
		local stacks = self:GetStackCount() + kv.stacks
		self:SetStackCount(stacks)
		self:SetDuration(self:CalcDuration(),true)
		self:CheckConversions()
	end
end

function element_elec:CheckConversions()
	if IsServer() then
		local hTarget = self:GetParent()
		local tModifiers = hTarget:FindAllModifiers()
		local tKeys = {stack = self:GetStackCount()}
		for __,hModifier in pairs(tModifiers) do
			if hModifier.ElecConversion then
				tKeys = hModifier:ElecConversion(tKeys)
			end
		end
		if tKeys.stack ~= self:GetStackCount() then self:SetStackCount(tKeys.stack) end
		self:SetDuration(self:CalcDuration(),true)
		if self:GetStackCount() < 1 then self:Destroy() end
	end
end

function element_elec:GetTexture()
	return "element_elec"
end

function element_elec:OnDestroy()
	if IsServer() then
		if self:GetStackCount() < 1 then return end
		self:DamageParent()
		local hCaster = self:GetCaster()
		local tTargets = FindUnitsInRadius(hCaster:GetTeam(),
			self:GetParent():GetAbsOrigin(),
			self:GetParent(),
			self:CalcDistance(),
			DOTA_UNIT_TARGET_TEAM_BOTH,
			DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_MECHANICAL,
			DOTA_UNIT_TARGET_FLAG_NONE,
			FIND_ANY_ORDER,
			false
		)
		local iStacks = math.floor(self:GetStackCount()/3*2)
		if iStacks > 1 then
			self:SetStackCount(iStacks)
			if #tTargets > 0 then
				for _,hTarget in pairs(tTargets) do
					if hTarget ~= nil and ( not hTarget:IsMagicImmune() ) and ( not hTarget:IsInvulnerable() ) and hTarget ~= self:GetParent() then
						if hTarget:GetTeam() ~= hCaster:GetTeam() or hTarget:HasModifier("element_water") then
						self:Shock(hTarget)
						break
						end
					end
				end
			end
		end
	end
end

function element_elec:Shock(hTarget)
	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_zuus/zuus_arc_lightning.vpcf", PATTACH_CUSTOMORIGIN, nil );
		local hCaster = self:GetCaster()
		local hParent = self:GetParent()
		ParticleManager:SetParticleControlEnt( nFXIndex, 0, hParent, PATTACH_POINT_FOLLOW, "attach_hitloc", hParent:GetOrigin() + Vector( 0, 0, 50 ), true );
		ParticleManager:SetParticleControlEnt( nFXIndex, 1, hTarget, PATTACH_POINT_FOLLOW, "attach_hitloc", hTarget:GetOrigin(), true );
		ParticleManager:ReleaseParticleIndex( nFXIndex );
		hTarget:AddNewModifier( self:GetCaster(), self:GetAbility(), "element_elec", { stacks = self:GetStackCount(), duration = self:CalcDuration()} )
	end
end

function element_elec:DamageParent()
	if IsServer() then
		local armor = self:GetParent():GetPhysicalArmorValue() * 0.75
		if armor < 1 then armor = 1 end
		local damage = {
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = self:GetStackCount() * armor ,
			damage_type = DAMAGE_TYPE_MAGICAL,
			ability = self:GetAbility()
		}
		local dmg = ApplyDamage( damage )
		if dmg >= 1 then
		local life_time = 2.0
		local digits = string.len( math.floor( dmg ) ) + 1
		local numParticle = ParticleManager:CreateParticle( "particles/msg_fx/msg_crit.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		ParticleManager:SetParticleControl( numParticle, 1, Vector( 0, math.floor( dmg ), 4 ) )
		ParticleManager:SetParticleControl( numParticle, 2, Vector( life_time, digits, 0 ) )
		ParticleManager:SetParticleControl( numParticle, 3, Vector( 0, 150, 255 ) )
		EmitSoundOnLocationWithCaster( self:GetParent():GetOrigin(), "Hero_Zuus.ArcLightning.Cast", self:GetCaster() )
		end
	end
end

function element_elec:IsHidden()
	return false
end

function element_elec:DestroyOnExpire()
	return true
end

function element_elec:CalcDuration()
	local _d = self:GetStackCount()*0.015
	if _d > 1.5 then _d = 1.5 end
	return _d
end

function element_elec:CalcDistance()
	local _d = self:GetStackCount() * 35
	if _d > 1200 then _d = 1200 end
	return _d
end
