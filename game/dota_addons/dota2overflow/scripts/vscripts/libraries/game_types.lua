-- GAMEMODELIB_

if GameModeLib == nil then
	GameModeLib = class({})
end
GAMEMODELIB_MODES = {
	[0] = "dota", --default dota
	[1] = "lms", --last man standing (ffa but only one life)
	[2] = "ffa", --free for all
	[3] = "tdm", --team death match
	[4] = "ts", --team survivor
	[5] = "vip", --kill enemy leader
	[6] = "dom", --capture points and dominate
	[7] = "ctf", --capture flag
	[8] = "bomb", --plant & defuse bomb
	[9] = "dbomb", --double bomb
	[10] = "ft", --freeze tag
	[11] = "one", --all against one
}

GAMEMODELIB_SCORE_LIMIT = 15
GAMEMODELIB_ROUND_TIME = 10
GAMEMODELIB_FORCE_DAY = false
GAMEMODELIB_FORCE_NIGHT = false
GAMEMODELIB_ROUND_WIN_LEVELS = 2
GAMEMODELIB_ROUND_LOSS_LEVELS = 2
GAMEMODELIB_ROUND_WIN_GOLD = 1500
GAMEMODELIB_ROUND_LOSS_GOLD = 1500

function GameModeLib:Init()

end
function GameModeLib:RoundStart()

end
function GameModeLib:RoundEnd()

end
function GameModeLib:Score()

end
function GameModeLib:Win()

end
function GameModeLib:Lose()

end