
		-- local damageTable = {
			-- victim = self:GetParent(),
			-- attacker = self:GetCaster(),
			-- damage = 2*self:GetStackCount(),
			-- damage_type = DAMAGE_TYPE_MAGICAL,
		-- }
		-- --print("Fire Damage: " .. 2*self:GetStackCount())

LinkLuaModifier("element_fire", "lua_mods/element_fire", LUA_MODIFIER_MOTION_NONE)
function ApplyDamageFire( info )
	local damage = info.damage
	local target = info.victim
	local attacker = info.attacker
	local source = info.source
	local modifier = false
	if target ~= nil then
	modifier = target:AddNewModifier(attacker, source, "element_fire", {
            stacks = damage
        })
	end
	return modifier
end

function ApplyDamageElectric( info )
	local damage = info.damage
	local target = info.victim
	local attacker = info.attacker
	local source = info.source
	local modifier = false
	if target ~= nil then
	modifier = target:AddNewModifier(attacker, source, "element_elec", {
            stacks = damage
        })
	end
	return modifier
end

function ApplyDamagePoison( info )
	local damage = info.damage
	local target = info.victim
	local attacker = info.attacker
	local source = info.source
	local modifier = false
	if target ~= nil then
	modifier = target:AddNewModifier(attacker, source, "element_poison", {
            stacks = damage
        })
	end
	return modifier
end