if lightning_god == nil then
	lightning_god = class({})
end

LinkLuaModifier( "generic_lua_stun", "lua_abilities/moddota_help/generic_stun.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "element_elec", "lua_mods/element_elec.lua", LUA_MODIFIER_MOTION_NONE)

function lightning_god:GetCooldown(iLevel)
	local cooldown = self.BaseClass.GetCooldown( self, iLevel )
	if self:GetCaster():HasScepter() then cooldown = self:GetSpecialValueFor("cooldown_scepter") end
	return cooldown
end

function lightning_god:OnSpellStart()
	local hCaster = self:GetCaster()
	local nTeam = hCaster:GetTeam()
	local tHeroes = HeroList:GetAllHeroes()
	local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_zuus/zuus_thundergods_wrath_start.vpcf", PATTACH_ABSORIGIN, self:GetCaster() )
	ParticleManager:SetParticleControl( nFXIndex, 0, self:GetCaster():GetAbsOrigin() + Vector(0,0,65)  )
	ParticleManager:SetParticleControl( nFXIndex, 1, self:GetCaster():GetAbsOrigin() + Vector(0,0,65)  )
	ParticleManager:SetParticleControl( nFXIndex, 2, self:GetCaster():GetAbsOrigin() + Vector(0,0,2000)  )
	EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_Zuus.GodsWrath.PreCast", self:GetCaster() )
	self.points = {}
	for k,v in pairs(tHeroes) do
		if v:GetTeam() ~= nTeam then
			self:BlastThatTarget(v)
		end
	end
end

function lightning_god:BlastThatTarget(hTarget)
		AddFOWViewer(self:GetCaster():GetTeamNumber(), hTarget:GetOrigin(), self:GetSpecialValueFor("radius")*2, 3, false) 
		EmitSoundOnLocationWithCaster( hTarget:GetOrigin(), "Hero_Zuus.LightningBolt", self:GetCaster() )
		local nFXIndexLng = ParticleManager:CreateParticle( "particles/units/heroes/hero_zuus/zuus_thundergods_wrath.vpcf", PATTACH_WORLDORIGIN, nil )
		ParticleManager:SetParticleControl( nFXIndexLng, 0, hTarget:GetAbsOrigin() + Vector(-200,-200,2000) )
		ParticleManager:SetParticleControl( nFXIndexLng, 1, hTarget:GetAbsOrigin() )
		local nFXIndex = ParticleManager:CreateParticle( "particles/shadow_raze_gen/raze.vpcf", PATTACH_WORLDORIGIN, nil )
		ParticleManager:SetParticleControl( nFXIndex, 0, hTarget:GetAbsOrigin() )
		ParticleManager:SetParticleControl( nFXIndex, 15, Vector( 0, 150, 250 ) )
		local stunDur = self:GetSpecialValueFor("duration")
		local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), hTarget:GetOrigin(), nil, self:GetSpecialValueFor("radius"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
		local shk_dmg = self:GetSpecialValueFor("shock_damage")
		local mgc_dmg = self:GetSpecialValueFor("mdamage")
		if #enemies > 0 then
			for _,enemy in pairs(enemies) do
				if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					enemy:AddNewModifier( self:GetCaster(), self, "generic_lua_stun", { duration = stunDur } )
					enemy:AddNewModifier( self:GetCaster(), self, "element_elec", { stacks = shk_dmg, duration = 0.25} )
					local damage = {
						victim = enemy,
						attacker = self:GetCaster(),
						damage = mgc_dmg ,
						damage_type = DAMAGE_TYPE_MAGICAL,
						ability = self
					}
					local dmg = ApplyDamage( damage )
				end
			end
		end
end


function lightning_god:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_NO_TARGET
	return behav
end
