if ult_fod_pure == nil then
	ult_fod_pure = class({})
end

function ult_fod_pure:GetBehavior()
	local hCaster = self:GetCaster()
	local behav = DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_CHANNELLED
	if hCaster:HasScepter() then
		behav = behav + DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
	end
	return behav
end


function ult_fod_pure:GetAOERadius()
	local hCaster = self:GetCaster()
	if hCaster:HasScepter() then
		return self:GetSpecialValueFor("aoe_scepter")
	end
	return 0
end

function ult_fod_pure:OnChannelFinish(bInterrupted)
		if bInterrupted then return end
    local hTarget = self:GetCursorTarget()
		local vTarget = self:GetCursorPosition()
    local hCaster = self:GetCaster()
								local Colour = {150,50,255}
				local sSound = "Hero_Lion.FingerOfDeath"
				EmitGlobalSound(sSound)
		if hCaster:HasScepter() then
			self:DealWithAoETargets(vTarget)
		else
			if hTarget == nil or hTarget:TriggerSpellAbsorb( self ) then
					return
			end
			self:DamageTarget (hTarget)
					local nFXIndex = ParticleManager:CreateParticle( "particles/ult_finger.vpcf", PATTACH_CUSTOMORIGIN, nil );
					ParticleManager:SetParticleControlEnt( nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1", self:GetCaster():GetOrigin() + Vector( 0, 0, 96 ), true );
					ParticleManager:SetParticleControlEnt( nFXIndex, 1, hTarget, PATTACH_POINT_FOLLOW, "attach_hitloc", hTarget:GetOrigin(), true );
					ParticleManager:SetParticleControl(nFXIndex, 15, Vector(Colour[1],Colour[2],Colour[3]))
				ParticleManager:ReleaseParticleIndex( nFXIndex );
		end

end

function ult_fod_pure:DealWithAoETargets (v)
				local Colour = {150,50,255}
		local nFXIndex = ParticleManager:CreateParticle( "particles/ult_finger.vpcf", PATTACH_CUSTOMORIGIN, nil );
		ParticleManager:SetParticleControlEnt( nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1", self:GetCaster():GetOrigin() + Vector( 0, 0, 96 ), true );
		ParticleManager:SetParticleControl( nFXIndex, 1, v );
		ParticleManager:SetParticleControl(nFXIndex, 15, Vector(Colour[1],Colour[2],Colour[3]))
	ParticleManager:ReleaseParticleIndex( nFXIndex );

	local hCaster = self:GetCaster()
	local aoe = self:GetSpecialValueFor("aoe_scepter")
	local enemies = FindUnitsInRadius( hCaster:GetTeamNumber(), v, nil, aoe, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )

	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsMagicImmune() ) and ( not enemy:IsInvulnerable() ) then
				self:DamageTarget (enemy)
						local nFXIndex = ParticleManager:CreateParticle( "particles/ult_finger.vpcf", PATTACH_CUSTOMORIGIN, nil );
						ParticleManager:SetParticleControl( nFXIndex, 0, v );
				ParticleManager:SetParticleControlEnt( nFXIndex, 1, enemy, PATTACH_POINT_FOLLOW, "attach_hitloc",  enemy:GetOrigin(), true );
			ParticleManager:SetParticleControl(nFXIndex, 15, Vector(Colour[1],Colour[2],Colour[3]))
		ParticleManager:ReleaseParticleIndex( nFXIndex );
			end
		end
	end
end

function ult_fod_pure:DamageTarget (hTarget)
	---[[

	--local dmg = self:GetSpecialValueFor("shock_damage")

		--]]
		--[[
	hTarget:AddNewModifier(self:GetCaster(), self, "element_elec", {
		stacks = dmg
	})
	--]]
	---[[
	local damage = {
		attacker = self:GetCaster(),
		damage = self:GetSpecialValueFor("pure_damage"),
		damage_type = self:GetAbilityDamageType(),
		ability = self,
		victim = hTarget
	}
	ApplyDamage( damage )
	--]]
end
