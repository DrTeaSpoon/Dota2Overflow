if form_elec_p == nil then
	form_elec_p = class({})
end

function form_elec_p:IsHidden()
	return (self:GetStackCount() < 1)
end

function form_elec_p:DeclareFunctions ()
	local func = {
		MODIFIER_EVENT_ON_ATTACK_LANDED
	}
	return func
end

function form_elec_p:DestroyOnExpire()
	return false
end

function form_elec_p:OnIntervalThink()
	if IsServer() then
		self:SetStackCount(0)
		self:StartIntervalThink(-1)
	end
end

function form_elec_p:ElecConversion (kv)
	if IsServer() then
		if self:GetAbility():GetLevel() < 1 then return kv end
		if self:GetParent():PassivesDisabled() then return kv end
		self:SetStackCount(self:GetStackCount() + kv.stack)
		kv.stack = 0
		self:StartIntervalThink(5)
		self:SetDuration(5,true)
		return kv
	end
end

function form_elec_p:OnAttackLanded(keys)
	if IsServer() then
		local hAbility = self:GetAbility()
		if hAbility:GetLevel() < 1 then return end
			if self:GetParent():PassivesDisabled() then return end
		if keys.attacker == self:GetParent() then
			if keys.target:IsBuilding() or keys.target:IsMagicImmune() then return end
			local _r = self:GetStackCount()
			if _r > 0 then
				self:OnIntervalThink()
				local _m = hAbility:GetSpecialValueFor("multi")
				if self:GetParent():HasScepter() then _m = hAbility:GetSpecialValueFor("multi_scepter") end
				_r = math.floor(_r*_m)
				if self:GetParent():IsIllusion() then _r = math.ceil(_r/2) end
				keys.target:AddNewModifier( self:GetCaster(), hAbility, "element_elec", { stacks = _r, duration = 0.25} )
			end
		end
	end
end
