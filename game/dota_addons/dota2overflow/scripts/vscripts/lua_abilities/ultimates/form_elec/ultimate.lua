if form_elec == nil then
	form_elec = class({})
end

LinkLuaModifier("element_elec", "lua_mods/element_elec", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier( "form_elec_p", "lua_abilities/ultimates/form_elec/ultimate_p_mod.lua", LUA_MODIFIER_MOTION_NONE )

function form_elec:GetBehavior()
	if self:GetCaster():HasScepter() then return DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE end
	local behav =  DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function form_elec:GetCooldown(iLevel)
	if self:GetCaster():HasScepter() then return self:GetSpecialValueFor("cd_scepter") end
	return 0
end

function form_elec:GetIntrinsicModifierName() return "form_elec_p" end

function form_elec:OnSpellStart (kv)
	self:GetCaster():AddNewModifier( self:GetCaster(), self, "element_elec", { stacks = self:GetSpecialValueFor("shock_scepter") } )
end
