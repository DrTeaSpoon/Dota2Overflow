if eldri_devotee_m == nil then
	eldri_devotee_m = class({})
end
function eldri_devotee_m:IsHidden()
	return false
end

function eldri_devotee_m:GetEffectName()
 return "particles/generic_gameplay/generic_silence.vpcf"
end

function eldri_devotee_m:GetEffectAttachType()
 return PATTACH_OVERHEAD_FOLLOW
end

function eldri_devotee_m:CheckState()
	local state = {
	[MODIFIER_STATE_SILENCED] = true,
	}
	return state
end
