if eldri_devotee == nil then
	eldri_devotee = class({})
end

LinkLuaModifier( "eldri_devotee_p", "lua_abilities/ultimates/eldri_devotee/ultimate_p_mod.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "eldri_devotee_m", "lua_abilities/ultimates/eldri_devotee/ultimate_m_mod.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "book_eldri_modifier", "lua_items/overflow/basics/eldri_book.lua", LUA_MODIFIER_MOTION_NONE )

function eldri_devotee:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
	return behav
end

function eldri_devotee:GetCastRange(vLocation,hTarget)
	local _r = self:GetSpecialValueFor( "range" )
	if self:GetCaster():HasScepter() then
		_r = self:GetSpecialValueFor( "range_scepter" )
	end
	return _r
end

function eldri_devotee:OnSpellStart()
    local hTarget = self:GetCursorTarget()
    local hCaster = self:GetCaster()
		if hCaster == nil or hTarget == nil or hTarget:TriggerSpellAbsorb( self ) then
				return
		end
    local nDev = self:GetCaster():FindModifierByName("book_eldri_modifier"):GetStackCount()
		local fDuration = self:GetSpecialValueFor( "duration" ) * nDev

    EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_Omniknight.GuardianAngel", self:GetCaster() )
    hTarget:AddNewModifier(hCaster, self, "eldri_devotee_m", {duration = fDuration})
end
function eldri_devotee:GetIntrinsicModifierName() return "eldri_devotee_p" end

function eldri_devotee:OnUpgrade()
	self:GetCaster():AddNewModifier( self:GetCaster(), self, "book_eldri_modifier", { stacks = 2 } )
end
