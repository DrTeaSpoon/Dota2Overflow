if eldri_devotee_p == nil then
	eldri_devotee_p = class({})
end

function eldri_devotee_p:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_DEATH
	}
	return funcs
end

function eldri_devotee_p:IsHidden()
	return true
end

function eldri_devotee_p:OnDeath (keys)
	if IsServer() then
		if self:GetAbility():GetLevel() < 1 then return end
		if keys.unit:GetTeamNumber() == self:GetParent():GetTeamNumber() then return end
		if keys.unit:IsRealHero() and self:GetParent():IsRealHero() then
			local _r = self:GetAbility():GetSpecialValueFor( "range" )
			if self:GetCaster():HasScepter() then
				_r = self:GetAbility():GetSpecialValueFor( "range_scepter" )
			end
			if CalcDistanceBetweenEntityOBB(keys.unit, self:GetParent()) < _r then
				self:GetParent():AddNewModifier( self:GetParent(), self:GetAbility(), "book_eldri_modifier", { stacks = self:GetAbility():GetLevel() } )
			end
		end
	end
end
