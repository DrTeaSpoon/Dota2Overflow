if ult_necro_master == nil then
	ult_necro_master = class({})
end

LinkLuaModifier("ult_necro_master_mod", "lua_abilities/ultimates/necro_master/modifier.lua", LUA_MODIFIER_MOTION_NONE)

function ult_necro_master:OnSpellStart()
  local vTarget = self:GetCursorPosition()
  local vOrigin = self:GetCaster():GetAbsOrigin()
  local _d = self:getDistance(vTarget,vOrigin)
  	local info =
  	{
  		Ability = self,
			EffectName = "particles/necro_master.vpcf",
    	vSpawnOrigin = caster:GetAbsOrigin(),
    	fDistance = _d,
    	fStartRadius = 64,
    	fEndRadius = 64,
    	Source = caster,
    	bHasFrontalCone = false,
    	bReplaceExisting = false,
    	iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_NONE,
    	iUnitTargetFlags = DOTA_UNIT_TARGET_FLAG_NONE,
    	iUnitTargetType = DOTA_UNIT_TARGET_NONE ,
    	fExpireTime = GameRules:GetGameTime() + 10.0,
  		bDeleteOnHit = true,
  		vVelocity = caster:GetForwardVector() * _d,
  		bProvidesVision = true,
  		iVisionRadius = 100,
  		iVisionTeamNumber = caster:GetTeamNumber()
  	}
  	projectile = ProjectileManager:CreateLinearProjectile(info)
  	--EmitSoundOn( "Item_Desolator.Target", self:GetCaster() )
end


function ult_necro_master:getDistance(a, b)
    local x, y, z = a.x-b.x, a.y-b.y, a.z-b.z
    return square(x*x+y*y+z*z)
end

function ult_necro_master:ApplyModifierInAoe(_s,_r,_d,_v)
		local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), _v, self:GetCaster(), _r, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
		if #enemies > 0 then
			for _,enemy in pairs(enemies) do
				if enemy ~= nil and ( not enemy:IsMagicImmune() ) and ( not enemy:IsInvulnerable() ) then
					enemy:AddNewModifier(self:GetCaster(), self, _s, { duration = _d })
				end
			end
		end
end

function ult_necro_master:OnProjectileHit( hTarget, vLocation )
  local _r = self:GetAbility():GetSpecialValueFor("radius")
  ApplyModifierInAoe("ult_necro_master_mod",_r,15,vLocation)
end
