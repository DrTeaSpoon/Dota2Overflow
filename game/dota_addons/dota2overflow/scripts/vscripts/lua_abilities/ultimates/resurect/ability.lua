if ult_resurect == nil then
	ult_resurect = class({})
end


LinkLuaModifier("ult_resurect_mod", "lua_abilities/ultimates/resurect/modifier.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("element_fire", "lua_mods/element_fire", LUA_MODIFIER_MOTION_NONE)

function ult_resurect:GetIntrinsicModifierName()
  return "ult_resurect_mod"
end

function ult_resurect:OnSpellStart()
	local hCaster = self:GetCaster()
  local hTarget = self:GetCursorTarget()
	hTarget:AddNewModifier( self:GetCaster(), self, "ult_resurect_mod", {duration = self:GetSpecialValueFor("duration_scepter")} )
end


function ult_resurect:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	if self:GetCaster():HasScepter() then
    behav = DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
  end
	return behav
end

function ult_resurect:GetCustomCastErrorTarget(hTarget)
	local hCaster = self:GetCaster()
  if hCaster == hTarget  then
    return "#dota_hud_error_cant_cast_on_self"
  elseif hTarget:GetTeam() ~= hCaster:GetTeam() then
    return "#dota_hud_error_cant_cast_on_enemy"
  elseif not hTarget:IsRealHero() then
    return "#dota_hud_error_only_cast_on_hero"
  else
    return "#dota_hud_error_only_cast_on_hero"
  end
end

function ult_resurect:CastFilterResultTarget(hTarget)
	local hCaster = self:GetCaster()
  if hCaster ~= hTarget  then
		if hTarget:GetTeam() == hCaster:GetTeam() then
			if  hTarget:IsRealHero() then
		    return UF_SUCCESS
			end
		end
	end
  if hTarget:GetTeam() ~= hCaster:GetTeam() then
    return UF_FAIL_ENEMY
  end
  return UF_FAIL_CUSTOM
end
