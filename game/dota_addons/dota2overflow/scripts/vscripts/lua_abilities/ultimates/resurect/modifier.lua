if ult_resurect_mod == nil then
	ult_resurect_mod = class({})
end

function ult_resurect_mod:OnCreated( kv )
	if IsServer() then
		self.needexplosion = false
	  if self:GetCaster() ~= self:GetParent() then
			self.nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/phoenix/phoenix_solar_forge/phoenix_solar_forge_ambient.vpcf", PATTACH_POINT_FOLLOW, self:GetParent() )
			ParticleManager:SetParticleControlEnt(self.nFXIndex, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
			self:AddParticle( self.nFXIndex, false, false, -1, false, false )
		end
	end
end

function ult_resurect_mod:OnRefresh( kv )
	if IsServer() then
	end
end

function ult_resurect_mod:OnDestroy()
	if IsServer() then
    --fire bomb! --abaddon_abad_happy_01
	end
end

function ult_resurect_mod:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_REINCARNATION,
    MODIFIER_EVENT_ON_RESPAWN
	}
	return funcs
end

function ult_resurect_mod:IsHidden()
  if self:GetCaster() == self:GetParent() then
		return true
	else
		return false
	end
end

function ult_resurect_mod:IsPurgable()
  if self:GetCaster() == self:GetParent() then
    return false
  else
	  return true
   end
end

function ult_resurect_mod:IsPurgeException()
  if self:GetCaster() == self:GetParent() then
    return false
  else
	  return true
   end
end

function ult_resurect_mod:GetAttributes()
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE + MODIFIER_ATTRIBUTE_MULTIPLE + MODIFIER_ATTRIBUTE_PERMANENT
end

function ult_resurect_mod:AllowIllusionDuplicate()
	return false
end

function ult_resurect_mod:ReincarnateTime (kv)
	if IsServer() then
		local hAbility = self:GetAbility()
		if hAbility:GetLevel() < 1 then return end
		local _t = self:GetAbility():GetSpecialValueFor("resurect_time")
	  if self:GetCaster() == self:GetParent() then
	    if hAbility:IsCooldownReady() then
				hAbility:StartCooldown(hAbility:GetCooldown(hAbility:GetLevel()))
			--	local sSound = "abaddon_abad_happy_01"
		--		EmitSoundOnLocationWithCaster(self:GetParent():GetOrigin(), sSound, self:GetParent() )
			self.needexplosion = true
	      return _t
	    end
	  else
			self.needexplosion = true
			self:SetDuration(_t*2, true)
	    return _t
	  end
	end
end

function ult_resurect_mod:OnRespawn (kv)
	if IsServer() then
		if kv.unit ~= self:GetParent() then return end
		if self.needexplosion == false then return end
		local hAbility = self:GetAbility()
		if hAbility:GetLevel() < 1 then return end
    if self:GetCaster() == self:GetParent() then
      self:Explosion()
    else
      self:Explosion()
      self:Destroy()
    end
	end
end

function ult_resurect_mod:Explosion()
		local dmg = self:GetAbility():GetSpecialValueFor("damage")
		local aoe = self:GetAbility():GetSpecialValueFor("radius")
		local enemies = FindUnitsInRadius( self:GetParent():GetTeamNumber(), self:GetParent():GetOrigin(), self:GetParent(), aoe, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
		if #enemies > 0 then
			for _,enemy in pairs(enemies) do
				if enemy ~= nil and ( not enemy:IsMagicImmune() ) and ( not enemy:IsInvulnerable() ) then
					enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "element_fire", {
						stacks = dmg
					})
					local damageTable = {
						victim = enemy,
						attacker = self:GetParent(),
						damage = dmg*20,
						damage_type = DAMAGE_TYPE_MAGICAL,
					}
					ApplyDamage(damageTable)
				end
			end
		end
	local sSound = "Hero_Phoenix.SuperNova.Explode"
	EmitSoundOnLocationWithCaster(self:GetParent():GetOrigin(), sSound, self:GetParent() )
	local particleName = "particles/units/heroes/hero_phoenix/phoenix_supernova_reborn.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, self:GetParent() )
	ParticleManager:SetParticleControl( pfx, 0, self:GetParent():GetOrigin() )
	ParticleManager:SetParticleControl( pfx, 2, Vector(aoe,0,0) )
	ParticleManager:ReleaseParticleIndex(pfx)
	self.needexplosion = false
end
