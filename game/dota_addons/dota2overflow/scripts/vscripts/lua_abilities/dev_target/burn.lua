if modifier_dev_burn == nil then
    modifier_dev_burn = class({})
end

function modifier_dev_burn:OnCreated(kv)
    if IsServer() then
		local hAbility = self:GetAbility()
		local fDuration = kv.duration
		local iDamage = hAbility:GetSpecialValueFor("bonus")
		local fRate = 0.1
		local iIntervals = fDuration/fRate
		local iIntervalDamage = iDamage/iIntervals
		self.dmg = iIntervalDamage
		self:OnIntervalThink()
		self:StartIntervalThink(fRate)
    end
end

function modifier_dev_burn:OnRefresh(kv)
    if IsServer() then
		local hAbility = self:GetAbility()
		local fDuration = kv.duration
		local fOldDuration = self:GetRemainingTime()
		self:SetDuration(fOldDuration + fDuration,true)
		local iDamage = hAbility:GetSpecialValueFor("bonus")
		local fRate = 0.1
		local iIntervals = fDuration/fRate
		local iIntervalDamage = iDamage/iIntervals
		if self.dmg < iIntervalDamage then
			self.dmg = iIntervalDamage
		end
		self:StartIntervalThink(fRate)
    end
end

function modifier_dev_burn:OnIntervalThink() 
    if IsServer() then
		local damage = {
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = self.dmg,
			damage_type = DAMAGE_TYPE_MAGICAL,
			ability = self:GetAbility()
		}
		ApplyDamage( damage )
    end
end

function modifier_dev_burn:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		MODIFIER_EVENT_ON_SPENT_MANA
	}
	return funcs
end

function modifier_dev_burn:GetModifierAttackSpeedBonus_Constant()
	return -50
end

function modifier_dev_burn:GetModifierMoveSpeedBonus_Percentage()
	return -50
end

function modifier_dev_burn:OnSpentMana(kv)
	if IsServer() then
		if kv.unit == self:GetParent() then
			local damage = {
				attacker = self:GetParent(),
				damage = kv.cost,
				damage_type = DAMAGE_TYPE_MAGICAL,
				ability = kv.ability,
				victim = self:GetParent()
			}
			ApplyDamage( damage )
		end
	end
end