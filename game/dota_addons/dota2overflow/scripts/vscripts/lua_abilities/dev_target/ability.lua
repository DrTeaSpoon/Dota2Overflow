if dev_target == nil then
	dev_target = class({})
end
LinkLuaModifier("modifier_dev_burn", "lua_abilities/dev_target/burn.lua", LUA_MODIFIER_MOTION_NONE)
function dev_target:OnSpellStart()
	local hTarget = self:GetCursorTarget()
	local hCaster = self:GetCaster()
	local sMod = "modifier_dev_burn"
	local fDuration = self:GetSpecialValueFor("duration")
	hTarget:AddNewModifier(hCaster, self, sMod, { duration = fDuration }) 
end