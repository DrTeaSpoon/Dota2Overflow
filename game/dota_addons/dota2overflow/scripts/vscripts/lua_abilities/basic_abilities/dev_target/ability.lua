if dev_target == nil then
	dev_target = class({})
end

LinkLuaModifier("dev_target_modifier", "lua_abilities/basic_abilities/dev_target/modifier.lua", LUA_MODIFIER_MOTION_NONE)

function dev_target:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_RUNE_TARGET + DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
	return behav
end

function dev_target:CastFilterResultTarget( hTarget ) -- hTarget is the targeted NPC.
    return self:CCastFilter( hTarget, false )
end

function dev_target:GetCustomCastErrorTarget( hTarget) -- hTarget is the targeted NPC. 
    return self:CCastFilter( hTarget, true )
end

function dev_target:OnSpellStart()
	print("valid item?")
end

function dev_target:CCastFilter( hTarget, bError )
    if IsServer() then
        local hCaster = self:GetCaster()
        local vOrigin = hCaster:GetAbsOrigin()
        if hTarget.IsAlive then
            if bError then
                return "#dota_hud_error_target_is_not_rune" --returning error from localization
            else
                return UF_FAIL_CUSTOM
            end
        end
        if not bError then
            return UF_SUCCESS
        end
    end
end