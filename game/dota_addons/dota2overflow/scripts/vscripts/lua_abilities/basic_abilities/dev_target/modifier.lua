if dev_target_modifier == nil then
    dev_target_modifier = class({})
end


function dev_target_modifier:GetAttributes()
    return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE
end


function dev_target_modifier:OnCreated(kv)
    if IsServer() then
    end
end

function dev_target_modifier:IsPurgable()
    return false
end