if poison_cloud == nil then
	poison_cloud = class({})
end

LinkLuaModifier( "poison_cloud_mod", "lua_abilities/basic_abilities/poison_cloud/modifier.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "poison_cloud_poison", "lua_abilities/basic_abilities/poison_cloud/poison.lua", LUA_MODIFIER_MOTION_NONE )

function poison_cloud:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
	return behav
end


function poison_cloud:GetManaCost()
	return self.BaseClass.GetManaCost( self, self:GetLevel() )
end

function poison_cloud:GetCooldown( nLevel )
	if self:GetCaster():HasScepter() then
		return self.BaseClass.GetCooldown( self, nLevel )
	else
		return self.BaseClass.GetCooldown( self, nLevel )
	end
end

function poison_cloud:GetAOERadius()
	return self:GetSpecialValueFor("radius")
end

function poison_cloud:OnSpellStart()
	 local vPos = self:GetCursorPosition() 
	 local particleName = "particles/units/heroes/hero_earth_spirit/espirit_spawn.vpcf"
	EmitSoundOnLocationWithCaster(vPos, "Hero_EarthSpirit.StoneRemnant.Destroy", self:GetCaster() )
	 local expl = ParticleManager:CreateParticle( particleName, PATTACH_WORLDORIGIN, self:GetCaster() )
	 ParticleManager:SetParticleControl( expl, 0, vPos )
	 ParticleManager:SetParticleControl( expl, 1, vPos )
	CreateModifierThinker( self:GetCaster(), self, "poison_cloud_mod", { duration = self:GetSpecialValueFor("duration"), rate = self:GetSpecialValueFor("rate") }, self:GetCursorPosition(), self:GetCaster():GetTeamNumber(), false )
end