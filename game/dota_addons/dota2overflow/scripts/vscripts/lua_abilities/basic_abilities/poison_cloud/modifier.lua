if poison_cloud_mod == nil then
	poison_cloud_mod = class({})
end

function poison_cloud_mod:GetEffectName()
	return "particles/poison_smoke.vpcf"
end
 
function poison_cloud_mod:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end
 
function poison_cloud_mod:OnCreated( kv )	
	if IsServer() then
		local rate = kv.rate or 1
		self.aoe = self:GetAbility():GetSpecialValueFor("radius")
		self.point = self:GetParent():GetAbsOrigin()
		self.team = self:GetCaster():GetTeamNumber()
		self:StartIntervalThink(rate)
	end
end

function poison_cloud_mod:OnIntervalThink()
	if IsServer() then
		local stack = self:GetAbility():GetSpecialValueFor("poison")
		local enemies = FindUnitsInRadius( self.team, self.point, nil, self.aoe, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
		if #enemies > 0 then
			for _,enemy in pairs(enemies) do
				if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "element_poison", { stacks = stack })
					if (not enemy:HasModifier("poison_cloud_poison")) then
						enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "poison_cloud_poison", {})
					end
				end
			end
		end
	end
end
 
function poison_cloud_mod:OnRefresh( kv )	
	if IsServer() then
	end
end

function poison_cloud_mod:OnDestroy()
	if IsServer() then
		UTIL_Remove( self:GetParent() )
	end
end

function poison_cloud_mod:IsHidden()
	return true
end

function poison_cloud_mod:IsPurgable() 
	return false
end

function poison_cloud_mod:IsPurgeException()
	return false
end

function poison_cloud_mod:GetAttributes()
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE 
end

function poison_cloud_mod:AllowIllusionDuplicate() 
	return false
end
