if poison_cloud_poison == nil then
	poison_cloud_poison = class({})
end
 
function poison_cloud_poison:OnCreated( kv )	
	if IsServer() then
	end
end
 
function poison_cloud_poison:OnRefresh( kv )	
	if IsServer() then
	end
end

function poison_cloud_poison:IsHidden()
	return false
end

function poison_cloud_poison:IsPurgable() 
	return true
end

function poison_cloud_poison:IsPurgeException()
	return false
end

function poison_cloud_poison:AllowIllusionDuplicate() 
	return true
end

function poison_cloud_poison:OnPoisonThink(tKeys)
	self:IncrementStackCount()
	return tKeys
end

function poison_cloud_poison:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,
		MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT
	}
 
	return funcs
end

function poison_cloud_poison:GetModifierMoveSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("speed") * self:GetStackCount()
end
function poison_cloud_poison:GetModifierAttackSpeedBonus_Constant()
	return self:GetAbility():GetSpecialValueFor("speed") * self:GetStackCount()
end