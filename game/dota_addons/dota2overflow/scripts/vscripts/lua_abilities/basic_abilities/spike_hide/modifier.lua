if spike_hide_mod == nil then
	spike_hide_mod = class({})
end

function spike_hide_mod:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_ATTACKED
	}
 
	return funcs
end

function spike_hide_mod:IsHidden()
	return false
end
function spike_hide_mod:IsPurgable() 
	return true
end
function spike_hide_mod:GetEffectName()
	return "particles/spike_shield_rock.vpcf"
end
 
--------------------------------------------------------------------------------
 
function spike_hide_mod:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end


function spike_hide_mod:OnAttacked(keys)
	if IsServer() then
		if keys.attacker ~= self:GetParent() and keys.target == self:GetParent()  then
			local hAbility = self:GetAbility()
			local fStun = hAbility:GetSpecialValueFor( "stun" )
			local iDamage = (hAbility:GetSpecialValueFor( "damage_return" )/100) * self:GetParent():GetPrimaryStatValue()
			if keys.attacker ~= nil and ( not keys.attacker:IsMagicImmune() ) and ( not keys.attacker:IsInvulnerable() ) then
				if keys.attacker:IsBuilding() then return end
				EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_Leshrac.Lightning_Storm", self:GetCaster() )
				keys.attacker:AddNewModifier( self:GetCaster(), self:GetAbility(), "generic_lua_stun", { duration = fStun } )
				keys.attacker:AddNewModifier( self:GetCaster(), self:GetAbility(), "element_elec", { stacks = iDamage, duration = 0.25} )
			end
		end
	end
end
