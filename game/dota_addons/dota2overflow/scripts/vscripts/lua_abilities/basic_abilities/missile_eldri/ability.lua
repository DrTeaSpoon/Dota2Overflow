if missile_eldri == nil then
	missile_eldri = class({})
end
LinkLuaModifier( "generic_lua_stun", "lua_abilities/moddota_help/generic_stun.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "book_eldri_modifier", "lua_items/overflow/basics/eldri_book.lua", LUA_MODIFIER_MOTION_NONE )

function missile_eldri:GetCastAnimation()
	return ACT_DOTA_CAST_ABILITY_1
end

function missile_eldri:OnSpellStart()
	local hTarget = self:GetCursorTarget()
	--hTarget:TriggerSpellReflect( self )
	local info = {
			EffectName = "particles/eldri_missile.vpcf",
			Ability = self,
			iMoveSpeed = self:GetSpecialValueFor( "speed" ),
			Source = self:GetCaster(),
			Target = self:GetCursorTarget(),
			iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_1
		}
	ProjectileManager:CreateTrackingProjectile( info )
	EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_Omniknight.GuardianAngel", self:GetCaster() )
end

function missile_eldri:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
	return behav
end

function missile_eldri:OnProjectileHit( hTarget, vLocation )
	--if hTarget ~= nil and ( not hTarget:IsInvulnerable() ) and ( not hTarget:IsMagicImmune() ) then
	if hTarget ~= nil and ( not hTarget:IsInvulnerable() ) and ( not hTarget:TriggerSpellAbsorb( self ) ) and ( not hTarget:IsMagicImmune() ) then
		EmitSoundOnLocationWithCaster( hTarget:GetOrigin(), "Hero_Omniknight.GuardianAngel", self:GetCaster() )
		local stun_dur = self:GetSpecialValueFor( "duration" )
		local magic_damage = self:GetSpecialValueFor( "m_damage" )
		local nDev = self:GetStacks()
		local devotion_damage = self:GetSpecialValueFor( "d_damage" ) * nDev
		hTarget:AddNewModifier( self:GetCaster(), self, "generic_lua_stun", { duration = stun_dur , stacking = 1 } )
		local damage = {
			attacker = self:GetCaster(),
			damage = magic_damage + devotion_damage,
			damage_type = DAMAGE_TYPE_MAGICAL,
			ability = self,
			victim = hTarget
		}
		ApplyDamage( damage )
	end
	--self:DebugModifiers()
	return true
end


function missile_eldri:OnUpgrade()
	if self:GetCaster():HasModifier("modifier_item_lotus_orb_active") then
		self:SetStolen(true)
	else
		self:AddStacks(1)
	end
end

function missile_eldri:AddStacks(_n)
	--if self.t_stack == nil then self.t_stack = 0 end
	self:GetCaster():AddNewModifier( self:GetCaster(), self, "book_eldri_modifier", { stacks = _n } )
	--self.t_stack = self.t_stack + _n
end

function missile_eldri:GetStacks()
	local hMod = self:GetCaster():FindModifierByName("book_eldri_modifier")
	local nDev = 0
	if hMod ~= nil then
		nDev = hMod:GetStackCount()
	end
	if self:IsStolen() then
		nDev = nDev + self:GetLevel()*2
	end
	return nDev
end

--function missile_eldri:OnUnStolen(  )
--	local hMod = self:GetCaster():FindModifierByName("book_eldri_modifier")
--	local nDev = hMod:GetStackCount()
--	if nDev == self.t_stack then
--		hMod:Destroy()
--	else
--		self:GetCaster():AddNewModifier( self:GetCaster(), self, "book_eldri_modifier", { stacks = self.t_stack*-1 } )
--	end
--end

function missile_eldri:DebugModifiers()
	local hCaster = self:GetCaster()
	local _n = hCaster:GetModifierCount()
	for i=0,_n do
		print(hCaster:GetModifierNameByIndex(i))
	end
end
