if shadow_walk_poison == nil then
	shadow_walk_poison = class({})
end
 
function shadow_walk_poison:OnCreated( kv )	
	if IsServer() then
	end
end
 
function shadow_walk_poison:OnRefresh( kv )	
	if IsServer() then
	end
end

function shadow_walk_poison:IsHidden()
	return false
end

function shadow_walk_poison:IsPurgable() 
	return true
end

function shadow_walk_poison:IsPurgeException()
	return false
end

function shadow_walk_poison:AllowIllusionDuplicate() 
	return true
end

function shadow_walk_poison:OnPoisonThink(tKeys)
	tKeys.damage_type = DAMAGE_TYPE_PURE
	return tKeys
end

function shadow_walk_poison:GetTexture()
	return "shadow_poison"
end