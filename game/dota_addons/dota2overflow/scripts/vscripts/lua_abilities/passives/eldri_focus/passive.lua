if eldri_focus == nil then
	eldri_focus = class({})
end

LinkLuaModifier( "eldri_focus_mod", "lua_abilities/passives/eldri_focus/p_mod.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "book_eldri_modifier", "lua_items/overflow/basics/eldri_book.lua", LUA_MODIFIER_MOTION_NONE )

function eldri_focus:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function eldri_focus:GetIntrinsicModifierName() return "eldri_focus_mod" end

function eldri_focus:OnUpgrade()
	self:GetCaster():AddNewModifier( self:GetCaster(), self, "book_eldri_modifier", { stacks = 1 } )
end
