if eldri_focus_mod == nil then
	eldri_focus_mod = class({})
end

function eldri_focus_mod:IsHidden()
	return true
end

function eldri_focus_mod:OnCreated()
	if IsServer() then
	end
end

function eldri_focus_mod:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MANA_REGEN_PERCENTAGE,
	}
	return funcs
end

function eldri_focus_mod:GetModifierPercentageManaRegen()
	--if IsServer() then
	return self:GetAbility():GetSpecialValueFor("mana")
	--end
end