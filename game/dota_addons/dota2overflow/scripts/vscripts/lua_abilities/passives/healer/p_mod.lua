if healer_mod == nil then
	healer_mod = class({})
end

function healer_mod:IsHidden()
	return true
end

function healer_mod:OnCreated()
	if IsServer() then
		local _r = 1
		self:StartIntervalThink(_r)
	end
end

function healer_mod:OnIntervalThink()
	if IsServer() then
		if self:GetAbility():GetLevel() < 1 then return end
		if not self:GetParent():IsAlive() then return end
		if self:GetParent():IsIllusion() then return end
		if not self:GetAbility():GetAutoCastState() then return end
		if not self:GetAbility():IsCooldownReady() then return end
		local aoe = self:GetAbility():GetSpecialValueFor("radius")
		local allies = FindUnitsInRadius( self:GetParent():GetTeamNumber(), self:GetParent():GetOrigin(), self:GetParent(), aoe, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
		if #allies > 0 then
			local _j = 0
			for _,ally in pairs(allies) do
				if ally ~= nil and ally:GetHealthDeficit() > 1 then
						_j = _j+1
				end
			end
			if _j > 0 then
				self:GetParent():CastAbilityNoTarget(self:GetAbility(), self:GetParent():GetPlayerID())
			end
		end
	end
end
