if healer == nil then
	healer = class({})
end

LinkLuaModifier( "book_eldri_modifier", "lua_items/overflow/basics/eldri_book.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "healer_mod", "lua_abilities/passives/healer/p_mod.lua", LUA_MODIFIER_MOTION_NONE )

function healer:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_AUTOCAST + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
	return behav
end

function healer:GetIntrinsicModifierName() return "healer_mod" end

function healer:OnUpgrade()
	self:GetCaster():AddNewModifier( self:GetCaster(), self, "book_eldri_modifier", { stacks = 1 } )
end

function healer:OnSpellStart (kv)
	local hp = self:GetSpecialValueFor("heal")
	local nDev = self:GetCaster():FindModifierByName("book_eldri_modifier"):GetStackCount()
	local aoe = self:GetSpecialValueFor("radius")
	local allies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetOrigin(), self:GetCaster(), aoe, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
	if #allies > 0 then
		local _j = 0
		for _,ally in pairs(allies) do
			if ally ~= nil and ally:GetHealthDeficit() > 1 then
				local nfx = ParticleManager:CreateParticle("particles/buff_apply_wisp.vpcf", PATTACH_CUSTOMORIGIN, nil );
				ParticleManager:SetParticleControl(nfx, 15, Vector(150,255,50))
				ParticleManager:SetParticleControlEnt( nfx, 0,	ally, PATTACH_POINT_FOLLOW, "attach_hitloc", ally:GetOrigin(), true )
				ParticleManager:SetParticleControlEnt( nfx, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetOrigin(), true )
				ally:Heal(hp*nDev, self)
					_j = _j+1
			end
		end
		if _j > 0 then
			EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_Omniknight.GuardianAngel", self:GetCaster() )
		end
	end
end
