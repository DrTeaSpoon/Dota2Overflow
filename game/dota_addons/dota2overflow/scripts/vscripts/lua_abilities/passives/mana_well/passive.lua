if mana_well == nil then
	mana_well = class({})
end

LinkLuaModifier( "mana_well_mod", "lua_abilities/passives/mana_well/p_mod.lua", LUA_MODIFIER_MOTION_NONE )

function mana_well:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_AUTOCAST + DOTA_ABILITY_BEHAVIOR_IMMEDIATE
	return behav
end

function mana_well:GetIntrinsicModifierName() return "mana_well_mod" end

function mana_well:OnSpellStart (kv)
	local mana = self:GetSpecialValueFor("mana")
	local aoe = self:GetSpecialValueFor("radius")
	local allies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetOrigin(), self:GetCaster(), aoe, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
	if #allies > 0 then
		local _j = 0
		for _,ally in pairs(allies) do
			if ally ~= nil  and ally:GetManaPercent() < 99 then
					local nfx = ParticleManager:CreateParticle("particles/buff_apply_wisp.vpcf", PATTACH_CUSTOMORIGIN, nil );
					ParticleManager:SetParticleControl(nfx, 15, Vector(150,255,255))
					ParticleManager:SetParticleControlEnt( nfx, 0,	ally, PATTACH_POINT_FOLLOW, "attach_hitloc", ally:GetOrigin(), true )
					ParticleManager:SetParticleControlEnt( nfx, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetOrigin(), true )
				ally:GiveMana(mana)
				_j = _j+1
			end
		end
		if _j > 0 then
			EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Item.LotusOrb.Destroy", self:GetCaster() )
		end
	end
end
