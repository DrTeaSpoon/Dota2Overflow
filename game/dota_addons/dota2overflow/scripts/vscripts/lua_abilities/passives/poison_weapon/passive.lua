if poison_weapon == nil then
	poison_weapon = class({})
end

LinkLuaModifier( "poison_weapon_mod", "lua_abilities/passives/poison_weapon/p_mod.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "poison_weapon_poison", "lua_abilities/passives/poison_weapon/poison.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier("element_poison", "lua_mods/element_poison", LUA_MODIFIER_MOTION_NONE)

function poison_weapon:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function poison_weapon:GetIntrinsicModifierName() return "poison_weapon_mod" end