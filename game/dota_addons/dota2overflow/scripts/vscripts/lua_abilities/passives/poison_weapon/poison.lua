if poison_weapon_poison == nil then
	poison_weapon_poison = class({})
end
 
function poison_weapon_poison:OnCreated( kv )	
	if IsServer() then
	end
end
 
function poison_weapon_poison:OnRefresh( kv )	
	if IsServer() then
	end
end

function poison_weapon_poison:IsHidden()
	return false
end

function poison_weapon_poison:IsPurgable() 
	return true
end

function poison_weapon_poison:IsPurgeException()
	return false
end

function poison_weapon_poison:AllowIllusionDuplicate() 
	return true
end

function poison_weapon_poison:OnPoisonThink(tKeys)
	tKeys.damage = tKeys.damage + self:GetAbility():GetSpecialValueFor("extra_damage")
	return tKeys
end

function poison_weapon_poison:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MISS_PERCENTAGE
	}
 
	return funcs
end

function poison_weapon_poison:GetModifierMiss_Percentage()
	return self:GetAbility():GetSpecialValueFor("miss")
end