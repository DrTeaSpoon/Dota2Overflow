if poison_weapon_mod == nil then
	poison_weapon_mod = class({})
end

function poison_weapon_mod:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_ATTACK_LANDED
	}
 
	return funcs
end

function poison_weapon_mod:IsHidden()
	return true
end

function poison_weapon_mod:OnAttackLanded(keys)
	if IsServer() then
		local hAbility = self:GetAbility()
			if hAbility:GetLevel() < 1 then return end
		if keys.attacker == self:GetParent() then
			if keys.target:IsBuilding() then return end
		local dmg = self:GetAbility():GetSpecialValueFor("bonus")
		if self:GetParent():IsIllusion() then dmg = math.ceil(dmg/2) end
			keys.target:AddNewModifier(self:GetCaster(), self:GetAbility(), "element_poison", { stacks = dmg })
			keys.target:AddNewModifier(self:GetCaster(), self:GetAbility(), "poison_weapon_poison", {})
			--EmitSoundOnLocationWithCaster(keys.target:GetOrigin(),  "Hero_Phoenix.poisonSpirits.Launch", keys.target) 
			
		end
	end
end