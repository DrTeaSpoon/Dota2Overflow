if item_ovf_difusal == nil then
	item_ovf_difusal = class({})
end
if item_ovf_difusal_2 == nil then
	item_ovf_difusal_2 = class({})
end
if item_ovf_difusal_3 == nil then
	item_ovf_difusal_3 = class({})
end
if item_ovf_difusal_4 == nil then
	item_ovf_difusal_4 = class({})
end
if item_ovf_difusal_5 == nil then
	item_ovf_difusal_5 = class({})
end

LinkLuaModifier( "item_ovf_difusal_mod", "lua_items/overflow/difusal/modifier.lua", LUA_MODIFIER_MOTION_NONE )

function item_ovf_difusal:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_difusal:GetIntrinsicModifierName()
	return "item_ovf_difusal_mod"
end

function item_ovf_difusal_2:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_difusal_2:GetIntrinsicModifierName()
	return "item_ovf_difusal_mod"
end

function item_ovf_difusal_3:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_difusal_3:GetIntrinsicModifierName()
	return "item_ovf_difusal_mod"
end

function item_ovf_difusal_4:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_difusal_4:GetIntrinsicModifierName()
	return "item_ovf_difusal_mod"
end

function item_ovf_difusal_5:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_difusal_5:GetIntrinsicModifierName()
	return "item_ovf_difusal_mod"
end
