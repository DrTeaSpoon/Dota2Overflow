if item_ovf_difusal_mod == nil then
	item_ovf_difusal_mod = class({})
end

function item_ovf_difusal_mod:DeclareFunctions()
	local funcs = {
	MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
	MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
		MODIFIER_PROPERTY_PROCATTACK_BONUS_DAMAGE_PHYSICAL
	}
	return funcs
end

function item_ovf_difusal_mod:OnCreated()
	if IsServer() then
	end
end

function item_ovf_difusal_mod:IsHidden()
	return true
end

function item_ovf_difusal_mod:GetAttributes()
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE + MODIFIER_ATTRIBUTE_MULTIPLE
end


function item_ovf_difusal_mod:GetModifierBonusStats_Agility()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("stat_agi")
	end
end

function item_ovf_difusal_mod:GetModifierBonusStats_Intellect()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("stat_int")
	end
end

function item_ovf_difusal_mod:GetModifierProcAttack_BonusDamage_Physical(keys)
	if IsServer() then
		local hAbility = self:GetAbility()
		if keys.attacker == self:GetParent() then
			local hCaster = keys.attacker
			local hTarget = keys.target
			if hTarget:IsBuilding() or hTarget:IsMagicImmune() then return end
			local agi = hCaster:GetAgility()
			local damage = (hAbility:GetSpecialValueFor("mana_burn")/100)*agi
			local mana = hTarget:GetMana()
			local overflow = mana - damage
			hTarget:ReduceMana(damage)
			if overflow < 0 then
				hTarget:Purge(true, false, false, false, false)
			end
			return (hTarget:GetMana() - mana)*-1
		end
	end
end

