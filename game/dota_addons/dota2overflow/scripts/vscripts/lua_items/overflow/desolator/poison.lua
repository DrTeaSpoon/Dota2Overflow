if item_ovf_desolator_poison == nil then
	item_ovf_desolator_poison = class({})
end
 
function item_ovf_desolator_poison:OnCreated( kv )	
	if IsServer() then
	end
end

function item_ovf_desolator_poison:OnDestroy() 	
	if IsServer() then
		local hAbility = self:GetAbility()
		if hAbility ~= nil then
			local hTarget = self:GetParent()
			if hTarget ~= nil and (not hTarget:IsAlive()) and hTarget:IsRealHero() then
				hAbility:SetCurrentCharges(hAbility:GetCurrentCharges() + 1)
			end
		end
	end
end
 
function item_ovf_desolator_poison:OnRefresh( kv )	
	if IsServer() then
	end
end

function item_ovf_desolator_poison:IsHidden()
	return false
end

function item_ovf_desolator_poison:IsPurgable() 
	return true
end

function item_ovf_desolator_poison:IsPurgeException()
	return false
end

function item_ovf_desolator_poison:AllowIllusionDuplicate() 
	return true
end

function item_ovf_desolator_poison:OnPoisonThink(tKeys)
	self:SetStackCount(tKeys.stack)
	return tKeys
end

function item_ovf_desolator_poison:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS
	}
 
	return funcs
end

function item_ovf_desolator_poison:GetModifierPhysicalArmorBonus()
	return self:GetAbility():GetSpecialValueFor("armor") * self:GetStackCount() * -1
end
