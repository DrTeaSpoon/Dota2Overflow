if item_ovf_desolator_mod == nil then
	item_ovf_desolator_mod = class({})
end

function item_ovf_desolator_mod:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_EVENT_ON_ATTACK_LANDED 
	}
	return funcs
end

function item_ovf_desolator_mod:OnCreated()
	if IsServer() then
	end
end

function item_ovf_desolator_mod:IsHidden()
	return true
end

function item_ovf_desolator_mod:GetAttributes()
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE + MODIFIER_ATTRIBUTE_MULTIPLE
end


function item_ovf_desolator_mod:GetModifierPreAttack_BonusDamage()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("dmg")
	end
end
function item_ovf_desolator_mod:OnAttackLanded(keys)
	if IsServer() then
		if keys.attacker == self:GetParent() then
			keys.target:AddNewModifier( self:GetCaster(), self:GetAbility(), "element_poison", { stacks = self:GetAbility():GetSpecialValueFor("poison_damage")} )
			keys.target:AddNewModifier( self:GetCaster(), self:GetAbility(), "item_ovf_desolator_poison", { } )
		end
	end
end

