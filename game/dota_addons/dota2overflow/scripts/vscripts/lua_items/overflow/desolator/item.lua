if item_ovf_desolator == nil then
	item_ovf_desolator = class({})
end

LinkLuaModifier( "item_ovf_desolator_mod", "lua_items/overflow/desolator/modifier.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "item_ovf_desolator_poison", "lua_items/overflow/desolator/poison.lua", LUA_MODIFIER_MOTION_NONE )

function item_ovf_desolator:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_desolator:GetIntrinsicModifierName()
	return "item_ovf_desolator_mod"
end