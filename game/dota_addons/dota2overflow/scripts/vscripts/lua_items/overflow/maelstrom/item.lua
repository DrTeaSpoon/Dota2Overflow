if item_ovf_maelstrom == nil then
	item_ovf_maelstrom = class({})
end

LinkLuaModifier( "item_ovf_maelstrom_modifier", "lua_items/overflow/maelstrom/modifier.lua", LUA_MODIFIER_MOTION_NONE )

function item_ovf_maelstrom:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_maelstrom:GetIntrinsicModifierName()
	return "item_ovf_maelstrom_modifier"
end