if item_ovf_venom_orb_poison == nil then
	item_ovf_venom_orb_poison = class({})
end
 
function item_ovf_venom_orb_poison:OnCreated( kv )	
	if IsServer() then
		self.reduction = 1 - self:GetAbility():GetSpecialValueFor("damage_reduction")/100
		self.stun = self:GetAbility():GetSpecialValueFor("stun_duration")
		self.stun_stack = 0
		self.chance = self:GetAbility():GetSpecialValueFor("chance")
	end
end
 
function item_ovf_venom_orb_poison:OnRefresh( kv )	
	if IsServer() then
		self.reduction = 1 - self:GetAbility():GetSpecialValueFor("damage_reduction")/100
		self.stun = self:GetAbility():GetSpecialValueFor("stun_duration")
		self.stun_stack = 0
		self.chance = self:GetAbility():GetSpecialValueFor("chance")
	end
end

function item_ovf_venom_orb_poison:IsHidden()
	return false
end

function item_ovf_venom_orb_poison:IsPurgable() 
	return true
end

function item_ovf_venom_orb_poison:IsPurgeException()
	return false
end

function item_ovf_venom_orb_poison:AllowIllusionDuplicate() 
	return true
end

function item_ovf_venom_orb_poison:OnPoisonThink(tKeys)
	tKeys.damage = tKeys.damage * self.reduction
	if RandomInt(1, 100) < self.chance then
		self:GetParent():AddNewModifier( self:GetCaster(), self:GetAbility(), "generic_lua_stun", { duration = self.stun, stacking = self.stun_stack } )
	end
	return tKeys
end
