if item_ovf_venom_orb == nil then
	item_ovf_venom_orb = class({})
end

LinkLuaModifier( "item_ovf_venom_orb_modifier", "lua_items/overflow/venom_orb/modifier.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "item_ovf_venom_orb_poison", "lua_items/overflow/venom_orb/poison.lua", LUA_MODIFIER_MOTION_NONE )

function item_ovf_venom_orb:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_venom_orb:GetIntrinsicModifierName()
	return "item_ovf_venom_orb_modifier"
end