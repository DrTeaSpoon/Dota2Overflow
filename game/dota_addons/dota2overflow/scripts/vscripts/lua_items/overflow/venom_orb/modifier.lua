if item_ovf_venom_orb_modifier == nil then
	item_ovf_venom_orb_modifier = class({})
end

function item_ovf_venom_orb_modifier:DeclareFunctions()
	local funcs = {
	MODIFIER_EVENT_ON_ATTACK_LANDED
	}
	return funcs
end

function item_ovf_venom_orb_modifier:IsHidden()
	return true
end

function item_ovf_venom_orb_modifier:GetAttributes() 
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE + MODIFIER_ATTRIBUTE_MULTIPLE
end

function item_ovf_venom_orb_modifier:OnAttackLanded(keys)
	if IsServer() then
		if keys.attacker == self:GetParent() then
			if keys.target:IsBuilding() or keys.target:IsMagicImmune() then return end
			keys.target:AddNewModifier( self:GetCaster(), self:GetAbility(), "element_poison", { stacks = self:GetAbility():GetSpecialValueFor("poison_damage")} )
			keys.target:AddNewModifier( self:GetCaster(), self:GetAbility(), "item_ovf_venom_orb_poison", { } )
		end
	end
end