if item_ovf_mjollnir == nil then
	item_ovf_mjollnir = class({})
end

LinkLuaModifier( "item_ovf_mjollnir_modifier", "lua_items/overflow/mjollnir/modifier.lua", LUA_MODIFIER_MOTION_NONE )

function item_ovf_mjollnir:GetBehavior() 
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_ovf_mjollnir:GetIntrinsicModifierName()
	return "item_ovf_mjollnir_modifier"
end