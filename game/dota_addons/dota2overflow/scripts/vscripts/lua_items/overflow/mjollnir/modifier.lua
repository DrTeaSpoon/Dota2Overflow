if item_ovf_mjollnir_modifier == nil then
	item_ovf_mjollnir_modifier = class({})
end

function item_ovf_mjollnir_modifier:DeclareFunctions()
	local funcs = {
	MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
	MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
	MODIFIER_EVENT_ON_ATTACK_LANDED
	}
	return funcs
end

function item_ovf_mjollnir_modifier:IsHidden()
	return true
end

function item_ovf_mjollnir_modifier:GetAttributes() 
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE + MODIFIER_ATTRIBUTE_MULTIPLE
end

function item_ovf_mjollnir_modifier:GetModifierPreAttack_BonusDamage()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("attack_damage")
	end
end

function item_ovf_mjollnir_modifier:GetModifierAttackSpeedBonus_Constant()
	if self:GetAbility() then
		return self:GetAbility():GetSpecialValueFor("attack_speed")
	end
end

function item_ovf_mjollnir_modifier:OnAttackLanded(keys)
	if IsServer() then
		if keys.attacker == self:GetParent() then
			if keys.target:IsBuilding() or keys.target:IsMagicImmune() then return end
			if RandomInt(1, 100) < self:GetAbility():GetSpecialValueFor("chance") then
				EmitSoundOnLocationWithCaster( self:GetCaster():GetOrigin(), "Hero_Zuus.ArcLightning.Cast", self:GetCaster() )
				keys.target:AddNewModifier( self:GetCaster(), self:GetAbility(), "element_elec", { stacks = self:GetAbility():GetSpecialValueFor("shock_damage"), duration = 0.25} )
			end
		end
	end
end