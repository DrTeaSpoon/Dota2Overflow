if item_difusal_ovf_mod == nil then
	item_difusal_ovf_mod = class({})
end

function item_difusal_ovf_mod:DeclareFunctions()
	local funcs = {-
MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
		MODIFIER_EVENT_ON_ATTACK_LANDED
	}
	return funcs
end

function item_difusal_ovf_mod:OnCreated()
	if IsServer() then
	end
end

function item_difusal_ovf_mod:IsHidden()
	return true
end

function item_difusal_ovf_mod:GetAttributes()
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE + MODIFIER_ATTRIBUTE_MULTIPLE
end

function item_difusal_ovf_mod:GetModifierPreAttack_BonusDamage()
	if self:GetAbility() then
	return self:GetAbility():GetSpecialValueFor("damage_bonus")
	else
	return 0
	end
end
function item_maelstrom_ovf_mod:OnAttackLanded(keys)
	if IsServer() then
		local hAbility = self:GetAbility()
		if keys.attacker == self:GetParent() then
		local hCaster = keys.attacker
		local hTarget = keys.target
			if hTarget:IsBuilding() or hTarget:IsMagicImmune() then return end
			if  RandomInt(1, 100) <= hAbility:GetSpecialValueFor("chance") then
				if hTarget:IsIllusion() then

				else
					local modifiers = hTarget:FindAllModifiers()
					for _,hModifier in pairs(modifiers) do
						local sName = hModifier:GetName()
						local fDur = hModifier:GetRemainingTime()
						local hAbility = hModifier:GetAbility()
						if hAbility ~= nil and hAbility:GetIntrinsicModifierName() ~= sName and fDur > 0.05 then
							hCaster:AddNewModifier(hCaster, hAbility, sName, {duration = fDur}})
							hModifier:Destroy()
							break
						end
					end
				end
			end
		end
	end
end
