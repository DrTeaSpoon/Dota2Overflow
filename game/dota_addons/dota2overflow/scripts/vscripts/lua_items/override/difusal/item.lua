if item_difusal_ovf == nil then
	item_difusal_ovf = class({})
end

LinkLuaModifier( "item_difusal_ovf_mod", "lua_items/overflow/difusal/modifier.lua", LUA_MODIFIER_MOTION_NONE )

function item_difusal_ovf:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_PASSIVE
	return behav
end

function item_difusal_ovf:GetIntrinsicModifierName()
	return "item_difusal_ovf_mod"
end

function item_difusal_ovf:OnItemEquipped(hItem)
	print("Equipped!" )
end
