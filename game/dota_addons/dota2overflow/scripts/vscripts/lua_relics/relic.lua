if item_relic_helix == nil then
	item_relic_helix = class({})
end


function item_relic_helix:GetBehavior()
	local behav = DOTA_ABILITY_BEHAVIOR_NO_TARGET + DOTA_ABILITY_BEHAVIOR_IMMEDIATE + DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_MOVEMENT + DOTA_ABILITY_BEHAVIOR_DONT_CANCEL_CHANNEL + DOTA_ABILITY_BEHAVIOR_IGNORE_PSEUDO_QUEUE + DOTA_ABILITY_BEHAVIOR_UNRESTRICTED
	return behav
end

function item_relic_helix:OnSpellStart()
	if self.castalready == nil then
  	self.castalready = true
		RelicPickups:GiveHeroNewRelic (self:GetCaster(),self:GetCurrentCharges())
	end
end
