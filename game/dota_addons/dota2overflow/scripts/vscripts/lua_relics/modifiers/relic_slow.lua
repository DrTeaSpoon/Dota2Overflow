if modifier_relic_slow == nil then
	modifier_relic_slow = class({})
end

function modifier_relic_slow:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE
	}

	return funcs
end

function modifier_relic_slow:GetModifierMoveSpeedBonus_Percentage()
	return -25
end

function modifier_relic_slow:IsHidden () return true end

function modifier_relic_slow:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
