if modifier_relic_night == nil then
	modifier_relic_night = class({})
end

function modifier_relic_night:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_BONUS_NIGHT_VISION
	}

	return funcs
end

function modifier_relic_night:GetBonusNightVision()
	return 700
end

function modifier_relic_night:IsHidden () return true end

function modifier_relic_night:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
