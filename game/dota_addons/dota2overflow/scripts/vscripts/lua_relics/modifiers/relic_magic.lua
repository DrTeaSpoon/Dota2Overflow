if modifier_relic_magic == nil then
	modifier_relic_magic = class({})
end

function modifier_relic_magic:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MAGICDAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE_STACKING
	}

	return funcs
end

function modifier_relic_magic:GetModifierMagicDamageOutgoing_Percentage()
	print("test")
	return 200
end

function modifier_relic_magic:GetModifierPercentageCooldownStacking()
	return 100
end

function modifier_relic_magic:IsHidden () return true end

function modifier_relic_magic:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
