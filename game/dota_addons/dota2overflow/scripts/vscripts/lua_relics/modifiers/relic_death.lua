if modifier_relic_death == nil then
	modifier_relic_death = class({})
end

function modifier_relic_death:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_DEATH,
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
	}

	return funcs
end

function modifier_relic_death:OnDeath (keys)
	if IsServer() then
		if keys.unit:GetTeamNumber() == self:GetParent():GetTeamNumber() then return end
		if keys.unit:IsRealHero() and self:GetParent():IsRealHero() then
			local _r = 1000
			if CalcDistanceBetweenEntityOBB(keys.unit, self:GetParent()) < _r then
				self:IncrementStackCount()
			end
		end
	end
end

function modifier_relic_death:IsHidden () return true end
function modifier_relic_death:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
function modifier_relic_death:GetModifierBonusStats_Strength() return self:GetStackCount()+1 end
function modifier_relic_death:GetModifierBonusStats_Agility() return self:GetStackCount()+1 end
function modifier_relic_death:GetModifierBonusStats_Intellect() return self:GetStackCount()+1 end
