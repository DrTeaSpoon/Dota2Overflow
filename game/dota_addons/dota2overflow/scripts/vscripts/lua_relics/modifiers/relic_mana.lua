if modifier_relic_mana == nil then
	modifier_relic_mana = class({})
end

function modifier_relic_mana:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MANA_REGEN_TOTAL_PERCENTAGE
	}

	return funcs
end

function modifier_relic_mana:GetModifierTotalPercentageManaRegen()
	return 2
end

function modifier_relic_mana:IsHidden () return true end

function modifier_relic_mana:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
