if modifier_relic_day == nil then
	modifier_relic_day = class({})
end

function modifier_relic_day:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_BONUS_DAY_VISION
	}

	return funcs
end

function modifier_relic_day:GetBonusDayVision()
	return 700
end

function modifier_relic_day:IsHidden () return true end

function modifier_relic_day:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
