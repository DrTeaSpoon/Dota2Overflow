if modifier_relic_blink == nil then
	modifier_relic_blink = class({})
end

function modifier_relic_blink:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_ATTACKED
	}

	return funcs
end

function modifier_relic_blink:OnCreated()
	if IsServer() then
		self.chance = 50
	end
end

function modifier_relic_blink:IsHidden () return true end
function modifier_relic_blink:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end

function modifier_relic_blink:OnAttacked(keys)
	if IsServer() then
		if keys.attacker ~= self:GetParent() and keys.target == self:GetParent() then
			if RandomInt(1, 100) < self.chance then
				self:Blink(self:GetParent(),RandomVector(500)+self:GetParent():GetAbsOrigin())
				self.chance = self.chance - 10
			else
				self.chance = self.chance + 10
			end
		end
	end
end

function modifier_relic_blink:Blink(hTarget, vPoint)
		local vOrigin = hTarget:GetAbsOrigin() --Our units's location
		ProjectileManager:ProjectileDodge(hTarget)  --We disjoint disjointable incoming projectiles.
		ParticleManager:CreateParticle("particles/items_fx/blink_dagger_start.vpcf", PATTACH_ABSORIGIN, hTarget) --Create particle effect at our caster.
		hTarget:EmitSound("DOTA_Item.BlinkDagger.Activate") --Emit sound for the blink
		hTarget:SetAbsOrigin(vPoint) --We move the caster instantly to the location
		FindClearSpaceForUnit(hTarget, vPoint, false) --This makes sure our caster does not get stuck
		ParticleManager:CreateParticle("particles/items_fx/blink_dagger_end.vpcf", PATTACH_ABSORIGIN, hTarget) --Create particle effect at our caster.
end
