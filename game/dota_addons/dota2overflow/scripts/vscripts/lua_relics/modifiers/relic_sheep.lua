if modifier_relic_sheep == nil then
	modifier_relic_sheep = class({})
end

function modifier_relic_sheep:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_ATTACKED
	}

	return funcs
end

function modifier_relic_sheep:OnCreated()
	if IsServer() then
		self.chance = 50
	end
end

function modifier_relic_sheep:IsHidden () return true end
function modifier_relic_sheep:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end

function modifier_relic_sheep:OnAttacked(keys)
	if IsServer() then
		if keys.attacker ~= self:GetParent() and keys.target == self:GetParent() then
			if RandomInt(1, 100) < self.chance then
				self:Hex(self:GetParent())
				self.chance = self.chance - 10
			else
				self:Hex(keys.attacker)
				self.chance = self.chance + 10
			end
		end
	end
end

function modifier_relic_sheep:Hex(hTarget)
	hTarget:AddNewModifier(self:GetParent(), nil, "modifier_sheepstick_debuff", {duration = 1.0})
end
