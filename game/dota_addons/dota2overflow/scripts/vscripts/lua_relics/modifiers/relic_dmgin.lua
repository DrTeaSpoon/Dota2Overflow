if modifier_relic_dmgin == nil then
	modifier_relic_dmgin = class({})
end

function modifier_relic_dmgin:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
	}

	return funcs
end

function modifier_relic_dmgin:GetModifierIncomingDamage_Percentage()
	return 150
end

function modifier_relic_dmgin:IsHidden () return true end

function modifier_relic_dmgin:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
