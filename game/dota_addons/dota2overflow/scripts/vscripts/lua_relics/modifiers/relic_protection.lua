if modifier_relic_protection == nil then
	modifier_relic_protection = class({})
end

function modifier_relic_protection:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE
	}

	return funcs
end

function modifier_relic_protection:OnCreated (kv)
	if IsServer() then
		self:SetStackCount(500)
		self:StartIntervalThink(1)
	end
end

function modifier_relic_protection:OnIntervalThink()
	if IsServer() then
		local limit = 500
		if self:GetParent():HasScepter() then limit = limit*2 end
		if self:GetStackCount() < limit then
			self:IncrementStackCount()
			self:IncrementStackCount()
		end
	end
end

function modifier_relic_protection:OnTakeDamage(keys)
	if IsServer() then

		if keys.attacker ~= self:GetParent() and keys.unit == self:GetParent() then
			if self:GetStackCount() > keys.damage then
				self:GetParent():Heal(keys.damage,self:GetParent())
				self:SetStackCount(self:GetStackCount() - keys.damage)
				EmitSoundOn( "Hero_Wisp.Tether.Stun", self:GetParent() )
			else
				self:GetParent():Heal(self:GetStackCount(),self:GetParent())
				self:SetStackCount(0)
			end
		end
	end
end

function modifier_relic_protection:IsHidden () return true end

function modifier_relic_protection:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
