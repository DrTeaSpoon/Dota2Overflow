if modifier_relic_cursed == nil then
	modifier_relic_cursed = class({})
end

function modifier_relic_cursed:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_DISABLE_HEALING
	}

	return funcs
end

function modifier_relic_cursed:GetDisableHealing()
	return 1
end

function modifier_relic_cursed:IsHidden () return true end

function modifier_relic_cursed:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
