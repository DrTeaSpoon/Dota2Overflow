if modifier_relic_dmgout == nil then
	modifier_relic_dmgout = class({})
end

function modifier_relic_dmgout:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE
	}

	return funcs
end

function modifier_relic_dmgout:GetModifierTotalDamageOutgoing_Percentage()
	return 150
end
function modifier_relic_dmgout:IsHidden () return true end

function modifier_relic_dmgout:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
