if modifier_relic_gold == nil then
	modifier_relic_gold = class({})
end

function modifier_relic_gold:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_BOUNTY_CREEP_MULTIPLIER,
		MODIFIER_PROPERTY_BOUNTY_OTHER_MULTIPLIER
	}

	return funcs
end

function modifier_relic_gold:GetModifierBountyCreepMultiplier (args)
	return 2
end

function modifier_relic_gold:GetModifierBountyOtherMultiplier (args)
	return 2
end

function modifier_relic_gold:IsHidden () return true end

function modifier_relic_gold:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
