if modifier_relic_speed == nil then
	modifier_relic_speed = class({})
end

function modifier_relic_speed:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE
	}

	return funcs
end

function modifier_relic_speed:GetModifierMoveSpeedBonus_Percentage()
	return 25
end

function modifier_relic_speed:IsHidden () return true end

function modifier_relic_speed:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
