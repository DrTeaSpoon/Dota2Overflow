if modifier_relic_heal == nil then
	modifier_relic_heal = class({})
end

function modifier_relic_heal:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE
	}

	return funcs
end

function modifier_relic_heal:GetModifierHealthRegenPercentage()
	return 2
end

function modifier_relic_heal:IsHidden () return true end

function modifier_relic_heal:OnDestroy()
	if IsServer() then
	  RelicPickups:HeroDied (self:GetParent())
	end
end
