if RelicPickups == nil then
  RelicPickups = class({})
end

function RelicPickups:Init (args)
  self:ListRelics()
  self.hero_pairing = {}
  self.hero_pair_id = {}
  self:StartSpawning()
  self.double_check = nil
end

function RelicPickups:StartSpawning (args)
  self.relic_count = 1
  for i = 1,#self.relic do
   local x,y = self:RandomLocation()
     for n = 1,self.relic_count do
       self:SpawnRelic(x,y,i)
     end
  end
end

function RelicPickups:ListRelics ()
  local tRelicList = LoadKeyValues("scripts/kv/relics.txt")
  local n = 1
  self.relic = {}
  for k,v in pairs(tRelicList) do
    self.relic[n] = v
    LinkLuaModifier( "modifier_relic_" .. v, "lua_relics/modifiers/relic_" .. v .. ".lua", LUA_MODIFIER_MOTION_NONE )
  	n = n + 1
  end
end

function RelicPickups:DropRelic(hTarget,x,y)
--  print("Trying to drop the relic")
  if self.hero_pair_id[hTarget] ~= nil  then
    if self.hero_pair_id[hTarget] > 0 then
  --    print("Trying to spawn(" .. self.hero_pair_id[hTarget] .. ") " .. self:GetRelic(self.hero_pair_id[hTarget]))
      self:SpawnRelic(x,y,self.hero_pair_id[hTarget])
    else
      print("was "..self.hero_pair_id[hTarget] )
    end
  else
  --  print("was nil")
  end

  if self.hero_pairing[hTarget] ~= nil  then
    if not self.hero_pairing[hTarget]:IsNull() then
    --  print("cleaning up modifier")
      self.hero_pairing[hTarget]:Destroy()
    end
  end
  self.hero_pair_id[hTarget] = 0
  CustomGameEventManager:Send_ServerToPlayer(hTarget:GetPlayerOwner() , "relics_new_relic", {relic="modifier_relic_none"})
end

function RelicPickups:GiveHeroNewRelic (hTarget,i)
  local _s = "modifier_relic_" .. self:GetRelic(i)
  --  print("giving player modifier (" .. i .. ") " .. _s)
  self:DropRelic(hTarget,self:RandomLocation())
  self.hero_pair_id[hTarget] = i
  --print("player pair_id = " .. self.hero_pair_id[hTarget] )
  self.hero_pairing[hTarget] = hTarget:AddNewModifier(hTarget, nil, _s, {})
  CustomGameEventManager:Send_ServerToPlayer(hTarget:GetPlayerOwner() , "relics_new_relic", {relic=_s})
end

function RelicPickups:HeroDied (hTarget)
  if hTarget:IsRealHero() and not hTarget:IsAlive() then
    local _v = hTarget:GetAbsOrigin()
    self:DropRelic(hTarget,_v.x,_v.y)
  end
end

function RelicPickups:GetRelic (i)
  if self.relic[i] ~= nil then
    return self.relic[i]
  else
    return self.relic[self:RandomRelic()]
  end
end

function RelicPickups:RandomRelic ()
  return math.random(1,#self.relic)
end

function RelicPickups:RandomLocation()
  local x = math.random(GetWorldMinX(),GetWorldMaxX())
  local y = math.random(GetWorldMinY(),GetWorldMaxY())
  return x,y
end

function RelicPickups:SpawnRelic(x,y,i)
--  print("spawning (" .. i .. ") " .. self:GetRelic(i))
  local hRune = CreateItem("item_relic_helix", nil, nil)
  hRune:SetCurrentCharges(i)
  hRune:SetPurchaseTime(0)
  local spawnPoint = Vector(x,y, 0) --+ randVector
  local destination = Vector(x,y, 0)
  local drop = CreateItemOnPositionSync(spawnPoint, hRune)
  hRune:LaunchLoot(true, 300, 0.75, destination)
--  GameRules:AddMinimapDebugPoint(1, spawnPoint, 1, 1, 1, 1, 2)
end
