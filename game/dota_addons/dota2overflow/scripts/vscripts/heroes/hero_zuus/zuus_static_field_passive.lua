if zuus_static_field_lua_passive == nil then
	zuus_static_field_lua_passive = class({})
end

function zuus_static_field_lua_passive:OnCreated( kv )	
	if IsServer() then
	end
end



function zuus_static_field_lua_passive:DeclareFunctions()
	local funcs = {
MODIFIER_EVENT_ON_ABILITY_EXECUTED 
	}
 
	return funcs
end

function zuus_static_field_lua_passive:OnAbilityExecuted(params)
	if IsServer() then
		if params.unit == self:GetParent() and not params.ability.IsProcBanned then
			local hAbility = self:GetAbility()
			if hAbility:GetLevel() < 1 then return end
			if params.ability:IsItem() then return end
			local iAoE = hAbility:GetSpecialValueFor( "radius" )
			local iDamage = hAbility:GetSpecialValueFor( "damage" )
			local iMDamage = hAbility:GetSpecialValueFor( "magic_damage" )
			
			local enemies = FindUnitsInRadius( self:GetParent():GetTeamNumber(), self:GetParent():GetOrigin(), self:GetParent(), iAoE, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsMagicImmune() ) and ( not enemy:IsInvulnerable() ) then
						enemy:AddNewModifier( self:GetCaster(), self:GetAbility(), "element_elec", { stacks = iDamage, duration = 0.25} )
						local damage = {
							victim = enemy,
							attacker = self:GetCaster(),
							damage = iMDamage ,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability = self:GetAbility()
						}
						local dmg = ApplyDamage( damage )
					end
				end
			end
		end
	end
end

function zuus_static_field_lua_passive:IsHidden()
	return true
end