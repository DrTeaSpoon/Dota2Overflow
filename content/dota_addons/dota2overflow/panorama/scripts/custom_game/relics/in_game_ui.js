"use strict";
var tool_tip = "modifier_relic_none";

function RelicChange(keys) {
  if (tool_tip == keys['relic']) {
    return;
  } else {
    var _s = "file://{images}/relics/" + keys['relic'] +".png";
    $("#relic_icon").SetImage(_s);
    tool_tip = keys['relic'];
  }
}

function ShowRelicTooltip() {
  var iconPanel = $("#relic_icon");
  var comLine = "relic_tooltip, file://{resources}/layout/relics/tooltip.xml , name="+tool_tip;
  $.DispatchEvent( "UIShowCustomLayoutParametersTooltip", "relic_tooltip", "file://{resources}/layout/custom_game/relics/tooltip.xml","name="+tool_tip);
//  $.DispatchEvent( "UIShowCustomLayoutParametersTooltip", "relic_tooltip", "file://{resources}/layout/tooltips/tooltip_custom_test.xml","name="+tool_tip);
}

function HideRelicTooltip() {
  var iconPanel = $("#relic_icon");
  var comLine = "relic_tooltip"
  $.DispatchEvent( "UIHideCustomLayoutTooltip", comLine);
}

(function () {
  $.Msg("[Relics] Init");
	 GameEvents.Subscribe( 'relics_new_relic', RelicChange );
  $.Msg("[Relics] Loaded");
})();
